# oradistrict.com - scripts related with articles from web page

## Getting started
Visit my webpage if you're looking for an Oracle Database articles about administration of the database.
https://www.oradistrict.com

## License
Use at your own discretion.

## Project status
It's ongoing project. More scripts will show up along with the articles.
